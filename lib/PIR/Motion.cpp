#include "Motion.h"

//Detect motion from prox sensor
boolean Motion::pirSensor(int MOTION_PIN)
{
    //One minute warmup period
    if (millis() >= 60000)
    {
        int proximity = digitalRead(MOTION_PIN);
        unsigned long currentMillis = millis();
        if (proximity == HIGH)
        {
            timeout = false;
            previousMotion = currentMillis;
            if (oldTimeout != timeout)
            {
                Serial.println(F("Motion detected"));
            }
        }
        else
        {
            //10 minute timeout
            if (currentMillis - previousMotion >= 600000)
            {
                timeout = true;
                if (oldTimeout != timeout)
                {
                    Serial.println(F("No motion detected for 10 minutes"));
                }
            }
        }
        oldTimeout = timeout;
    }
    return timeout;
}