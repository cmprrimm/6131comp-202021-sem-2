#include "http.h"

//Send readings to sub array
void HTTP::subReadingsArray(int currentTemp, int proximityReading)
{
    subArray.push_back(currentTemp);
    subArray.push_back(proximityReading);
    unsigned int size = subArray.size();
    //Push readings to server once array is size 6 (30 seconds)
    if (size == 6)
    {
        httpClient(size);
        //After readings are sent delete array to restart count
        subArray.clear();
        Serial.println("Readings sent to server");
    }
}

//Push the readings to the server
void HTTP::httpClient(unsigned int size)
{
    HTTPClient hClient;
    //Loop through sub array until all positions are sent
    int i = 0;
    while (i < size)
    {
        String fullURL = HOST;
        fullURL.concat(subArray[i]);
        i++;
        fullURL.concat("&p=");
        fullURL.concat(subArray[i]);
        i++;
        hClient.begin(fullURL);

        const char *headers[] = {"Date", "Server"};
        hClient.collectHeaders(headers, 2);
        int retCode = hClient.GET();

        if (retCode > 0)
        {
            Serial.print("HTTP ");
            Serial.println(retCode);
            if (retCode == HTTP_CODE_OK)
            {
                Serial.println("------");
                Serial.println("Date = " + hClient.header("Date"));
                Serial.println("Server = " + hClient.header("Server"));
                Serial.println("------");
                Serial.println(hClient.getString());
            }
        }
        else
        {
            Serial.println("Error... ");
            Serial.println(HTTPClient::errorToString(retCode));
        }
    }
}