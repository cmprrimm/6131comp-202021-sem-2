#include <Arduino.h>
#include <HardwareSerial.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <vector>

//Setup HTTP class
class HTTP
{
private:
    const char *HOST = "https://rsmembedded.000webhostapp.com/RSM%20Embeded%20server.php?group=RSM&t=";
    std::vector<int> subArray;

public:
    void subReadingsArray(int currentTemp, int proximityReading);
    void httpClient(unsigned int size);
};