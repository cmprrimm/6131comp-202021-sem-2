#include "Check.h"

//Inital sensor check during setup
void Check::sensorCheck(int temp, int proximity, int MOTION_PIN)
{
    bool proxCheck = false;
    //Check temp is either a number or is less than 100
    if (isnan(temp) || temp > 100)
    {
        Serial.println(F("PROBLEM with Temperature Sensor"));
        Serial.println(F("Please check the Temperature Sensor and try again"));
        exit(1);
    }
    else
    {
        Serial.println(F("Temperature Sensor READY"));
    }

    //Checks prox detects a change from HIGH/LOW to LOW/HIGH
    if (proximity == HIGH)
    {
        while (millis() < 20000)
        {
            if (digitalRead(MOTION_PIN) == LOW)
            {
                proxCheck = true;
                break;
            }
        }
        if (proxCheck)
        {
            Serial.println("Proximity sensor is READY");
            Serial.println("");
        }
        else
        {
            Serial.println("Proximity sensor ERROR");
            Serial.println("");
            exit(1);
        }
    }
    else
    {
        while (millis() < 20000)
        {
            if (digitalRead(MOTION_PIN) == HIGH)
            {
                proxCheck = true;
                break;
            }
        }
        if (proxCheck)
        {
            Serial.println("Proximity sensor is READY");
            Serial.println("");
        }
        else
        {
            Serial.println("Proximity sensor ERROR");
            Serial.println("");
            exit(1);
        }
    }
}