#include <Arduino.h>
#include <HardwareSerial.h>
#include <vector>
#include <SPIFFS.h>

//Create storage class
class Storage
{
private:
    std::vector<int> arr;
    String filename = "/readings.txt";

public:
    void mainReadingsArray(int currentTemp, int proximityReading);
    void memoryStore(int temp, int proximity);
    void readFile();
};