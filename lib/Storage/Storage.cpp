#include "Storage.h"

//Add readings to main storage array
void Storage::mainReadingsArray(int currentTemp, int proximityReading)
{
    arr.push_back(currentTemp);
    arr.push_back(proximityReading);
}

//Add array to txt file and store in SPIFFS
void Storage::memoryStore(int temp, int proximity)
{
    Serial.println("File write begin");
    unsigned int size = arr.size();
    File file = SPIFFS.open(filename, FILE_APPEND);
    //Make sure file is open
    if (!file)
    {
        Serial.println("Error opening file");
        return;
    }

    int i = 0;
    //Loop array and print to file
    while (i < size)
    {
        file.println(arr[i]);
        i++;
    }
    file.close();
    Serial.println("File write end");
    //Clear array as now in memory
    arr.clear();
}

//Print file to console
void Storage::readFile()
{
    File file = SPIFFS.open(filename, FILE_READ);
    if (!file)
    {
        Serial.println("Error opening file");
        return;
    }
    while (file.available())
    {
        Serial.print(file.readString());
        Serial.println("--");
    }
    file.close();
}