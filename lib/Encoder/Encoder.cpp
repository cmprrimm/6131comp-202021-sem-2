#include "Encoder.h"

//Setup rotary encoder
Encoder::Encoder(int pinA, int pinB)
{
  ESP32Encoder::useInternalWeakPullResistors = UP;
  encoder.attachSingleEdge(pinA, pinB);
  encoder.setCount(10);
}

// range limits and checks the encoder
int Encoder::rangeCheck()
{
  if (encodercnt != encoder.getCount())
  {
    encodercnt = encoder.getCount();

    if (encodercnt >= 28)
    {
      encodercnt = 28;
      encoder.setCount(encodercnt);
    }
    else if (encodercnt <= 10)
    {
      encodercnt = 10;
      encoder.setCount(encodercnt);
    }
    Serial.println(encodercnt);
  }
  return encodercnt;
}