#include <ESP32Encoder.h>

//Setup encoder class
class Encoder
{
private:
    int encodercnt = 0;
    ESP32Encoder encoder;

public:
    Encoder(int pinA, int pinB);
    int rangeCheck();
};