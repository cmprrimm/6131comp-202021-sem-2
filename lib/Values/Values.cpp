#include "Values.h"

//Print values to console
void Values::latestValues(int temp, int proximity)
{
    Serial.print(F("The current Temprature is: "));
    Serial.println(temp);

    //Check PIR sensor is warmed up
    if (millis() >= 60000)
    {
        if (proximity == HIGH)
        {
            Serial.println(F("The proximity sensor is DETECTING motion"));
            Serial.println("");
        }
        else
        {
            Serial.println(F("The proximity sensor is NOT DETECTING motion"));
            Serial.println("");
        }
    }
    else
    {
        Serial.println(F("The proximity sensor is warming up"));
    }
}