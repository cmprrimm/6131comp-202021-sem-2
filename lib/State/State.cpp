#include "State.h"

//Sets min and max temp default values
int minTemp = 10;
int maxTemp = 28;

//Update temp from website
void Temp::tempUpdate(String newMin, String newMax)
{
    inputMin = atoi(newMin.c_str());
    inputMax = atoi(newMax.c_str());
    minTemp = inputMin;
    maxTemp = inputMax;
}

//Sets LED colour
LEDState State::state(int MOTION_PIN, int temp, boolean timeout)
{
    //If 10 minute timeout period of no motion then flash LED orange
    if (timeout)
    {
        if (millis() - lastChangeTime >= specifiedDelay)
        {
            if (current != LEDState::OFF)
                current = LEDState::OFF;
            else
                current = LEDState::ON;
        }
    }
    else if (temp < minTemp)
        //If temp less than minTemp turn on heating (Red LED)
        current = LEDState::RED;
    else if (temp >= minTemp && temp <= maxTemp)
        //If temp between min and max temps then desired temp achieved (Green LED)
        current = LEDState::GREEN;
    else if (temp > maxTemp)
        //If temp greater than maxTemp turn on AC (Blue LED)
        current = LEDState::BLUE;

    //If there has been a state change update LED and print to console
    if (previousState != current)
    {
        if (current == LEDState::RED)
        {
            ledcWrite(1, 100), ledcWrite(3, 0), ledcWrite(2, 0);
            Serial.println(F("Heating has been turned ON"));
        }
        else if (current == LEDState::GREEN)
        {
            ledcWrite(1, 0), ledcWrite(3, 0), ledcWrite(2, 100);
            Serial.println(F("The desired tempertaure has been met"));
        }
        else if (current == LEDState::BLUE)
        {
            ledcWrite(1, 0), ledcWrite(3, 100), ledcWrite(2, 0);
            Serial.println(F("The AC has been turned ON"));
        }
        else if (current == LEDState::ON)
        {
            ledcWrite(1, 100), ledcWrite(2, 50), ledcWrite(3, 0);
        }
        else if (current == LEDState::OFF)
        {
            ledcWrite(1, 0), ledcWrite(2, 0), ledcWrite(3, 0);
        }
        previousState = current;
        if (old != current)
            lastChangeTime = millis();
    }
    return current;
}

//Setup rotary encoder
const int ROTARY_A = 5;
const int ROTARY_B = 4;
Encoder encoder(ROTARY_A, ROTARY_B);

//Check if rotary button has been pressed
void Button::checkButton(int ROTARY_CLICK)
{
    if (digitalRead(ROTARY_CLICK) != lastState)
    {
        //If button pressed
        lastState = digitalRead(ROTARY_CLICK);
        if (lastState == LOW)
        {
            //Print current min temp
            btnCount++;
            Serial.print(F("The current Minimum Temperature is: "));
            Serial.println(minTemp);
            Serial.println(F("Change Minimum Temperature to: "));
            while (btnCount == 1)
            {
                //until button pressed again read rotary twist
                encoder.rangeCheck();
                if (digitalRead(ROTARY_CLICK) != lastState)
                {
                    lastState = digitalRead(ROTARY_CLICK);
                    if (lastState == LOW)
                    {
                        //If rotary pressed again leave loop
                        btnCount++;
                    }
                }
            }
            //Set new min temp and print to console
            minTemp = encoder.rangeCheck();
            Serial.print(F("The new Minimum Temperature is: "));
            Serial.println(minTemp);
            //Print current max temp
            Serial.print(F("The current Maximum Temperature is: "));
            Serial.println(maxTemp);
            Serial.println(F("Change Maximum Temperature to: "));
            while (btnCount == 2)
            {
                //until button pressed again read rotary twist
                encoder.rangeCheck();
                if (digitalRead(ROTARY_CLICK) != lastState)
                {
                    lastState = digitalRead(ROTARY_CLICK);
                    if (lastState == LOW)
                    {
                        //If rotary pressed check max temp is more than min temp
                        if (encoder.rangeCheck() < minTemp)
                        {
                            //If max temp less than min temp then print error message and take reading again
                            Serial.println("Maximum Temperature cannot be below Minimum Temperature!");
                            Serial.println("Please enter a new Maximum Temperature which is above the Minimum Temperature");
                        }
                        else
                        {
                            //If validation passed exit loop
                            btnCount++;
                        }
                    }
                }
            }
            //Set new max temp and print to console
            maxTemp = encoder.rangeCheck();
            lastState = digitalRead(ROTARY_CLICK);
            Serial.print(F("The new Maximum Temperature is: "));
            Serial.println(maxTemp);
            btnCount = 0;
        }
    }
}