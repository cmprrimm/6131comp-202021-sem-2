#include <Arduino.h>
#include <HardwareSerial.h>
#include <Encoder.h>
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

//Create enum of different LED states
enum LEDState
{
  RED,
  BLUE,
  GREEN,
  ON,
  OFF
};

//Create temp class
class Temp
{
private:
  int inputMin;
  int inputMax;

public:
  void tempUpdate(String newMin, String newMax);
};

//Create state class
class State
{
private:
  LEDState current;
  int previousState;
  unsigned long lastChangeTime;
  LEDState old = current;
  int specifiedDelay = 500;

public:
  LEDState state(int MOTION_PIN, int temp, boolean timeout);
  boolean timeDiff(unsigned long start, int specidiedDelay);
};

//create Button class
class Button
{
private:
  int lastState;
  int btnCount = 0;

public:
  void checkButton(int ROTARY_CLICK);
};