#include <DHT.h>
#include <Check.h>
#include <Values.h>
#include <State.h>
#include <Motion.h>
#include <http.h>
#include <Storage.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

//Set the pin numbers for the ESP32 connections
const int RED_PIN = 15;
const int GREEN_PIN = 16;
const int BLUE_PIN = 17;
const int ROTARY_CLICK = 2;
const int MOTION_PIN = 21;

//Setup webserver for Feature H
AsyncWebServer server(80);

//Internet connection ID and Password
const char *SSID = "MSI 7542";
const char *PASS = "Robby199";

//Params for the website of Feature H
const char *PARAM_INPUT_1 = "minTemp";
const char *PARAM_INPUT_2 = "maxTemp";

//HTML code for Feature H
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>ESP Input Form</title>
  <meta name="viewpoint" content="width=device-width, initial-scale=1">
  </head><body>
  <form action="/get">

        <h3>Please enter Minimum Temperature into the box below</h3>
        <input type="number" name="minTemp" id="minTemp" min="10" max="28">
        <h3>Please enter Maximum Temperature into the box below</h3>
        <input type="number" name="maxTemp" id="maxTemp" min="10" max="28">
        <br>
        <br>
        <input type="submit" onSubmit="return tempComp()" value="Submit">
  </form>
  </body></html>)rawliteral";

//Function to check webserver exists
void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Not found");
}

//Setup new check object
Check check;

//Setup new temp object
Temp newTemp;

//Setup new value object
Values values;

//Setup new state object
State state;

//Setup new button object
Button button;

//Setup new motion object
Motion motion;

//Setup new HTTP object
HTTP http;

//Setup new storage object
Storage storage;

//DHT11 setup
#define DHTPIN 14
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

//Sets LED pins to be analogue
void esp32Setup()
{
  ledcAttachPin(RED_PIN, 1);
  ledcSetup(1, 12000, 8);
  ledcAttachPin(GREEN_PIN, 2);
  ledcSetup(2, 12000, 8);
  ledcAttachPin(BLUE_PIN, 3);
  ledcSetup(3, 12000, 8);
}

//Setup method for pins and wifi as well as website server
void setup()
{
  Serial.begin(115200);
  esp32Setup();
  dht.begin();
  pinMode(MOTION_PIN, INPUT_PULLUP);
  pinMode(ROTARY_CLICK, INPUT_PULLUP);
  if (!SPIFFS.begin(true))
  {
    Serial.println("Failure to mount SPIFFS");
  }

  Serial.print("Connecting to ");
  Serial.println(SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PASS);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(250);
    Serial.println(".");
  }
  Serial.print("Connected as :");
  Serial.println(WiFi.localIP());

  // Send web page with input fields to client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/html", index_html);
  });

  // Send a GET request to <ESP_IP>
  server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request) {
    String inputMessageMin;
    String inputMessageMax;
    String inputParam;
    // GET input values
    if (request->hasParam(PARAM_INPUT_1) && request->hasParam(PARAM_INPUT_2))
    {
      inputMessageMin = request->getParam(PARAM_INPUT_1)->value();
      inputMessageMax = request->getParam(PARAM_INPUT_2)->value();
      if (inputMessageMin > inputMessageMax)
      {
        request->send(200, "text/html", "ERROR!!.. The minimum temperature cannot be greater than the maximum temperature, click return to Home Page and retry. <br><a href=\"/\">Return to Home Page</a>");
      }
      else
      {
        request->send(200, "text/html", "HTTP GET request sent to your ESP with Minimum Temperature of: " + inputMessageMin + " and Maximum Temperature of: " + inputMessageMax + "<br><a href=\"/\">Return to Home Page</a>");
        newTemp.tempUpdate(inputMessageMin, inputMessageMax);
        Serial.println("The new Minimum Temperature is: " + inputMessageMin + " and the new Maximum Temperature is: " + inputMessageMax);
      }
    }
    else
    {
      inputMessageMin = "No message sent";
      inputParam = "none";
      request->send(200, "text/html", "HTTP GET request sent to your ESP on input field (" + inputParam + ") with value: " + inputMessageMin + "<br><a href=\"/\">Return to Home Page</a>");
    }
  });
  server.onNotFound(notFound);
  server.begin();
  int temp = dht.readTemperature();
  int proximity = digitalRead(MOTION_PIN);

  //Check sensor are working
  check.sensorCheck(temp, proximity, MOTION_PIN);
}

static unsigned long previousMillis;
static unsigned long previousServerMillis;
static unsigned long previousFlashMillis;
void loop()
{
  //Take temp and prox readings
  int temp = (int)dht.readTemperature();
  int proximity = digitalRead(MOTION_PIN);

  //Check if rotary button has been pressed
  button.checkButton(ROTARY_CLICK);
  //Check if 10 minutes has elapsed since last motion
  boolean timeout = motion.pirSensor(MOTION_PIN);

  unsigned long currentMillis = millis();

  //Print values every 5 seconds to console
  if (currentMillis - previousMillis >= 5000)
  {
    values.latestValues(temp, proximity);
    previousMillis = currentMillis;
  }

  //Store readings every 10 seconds and send to server every 30 seconds
  if (currentMillis - previousServerMillis >= 10000)
  {
    storage.mainReadingsArray(temp, proximity);
    http.subReadingsArray(temp, proximity);
    previousServerMillis = currentMillis;
  }

  //Store readings in SPIFFS every 2 mins
  if (currentMillis - previousFlashMillis >= 120000)
  {
    storage.memoryStore(temp, proximity);
    //storage.readFile();
    previousFlashMillis = currentMillis;
  }

  //Update current state of LED depending on temp and prox
  state.state(MOTION_PIN, temp, timeout);
}